<?php
namespace HIVE\HiveExtApi\Controller;

/***
 *
 * This file is part of the "hive_ext_api" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * RequestController
 */
class RequestController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * requestRepository
     *
     * @var \HIVE\HiveExtApi\Domain\Repository\RequestRepository
     * @inject
     */
    protected $requestRepository = null;

    /**
     * @var \HIVE\HiveExtApi\Mvc\View\JsonView
     */
    protected $view = null;

    /**
     * @var string
     */
    protected $defaultViewObjectName = 'HIVE\\HiveExtApi\\Mvc\\View\\JsonView';

    /**
     * action request
     *
     * @return void
     */
    public function requestAction()
    {
        return $this->view->render();
    }
}
