
plugin.tx_hiveextapi_request {
    view {
        # cat=plugin.tx_hiveextapi_request/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_api/Resources/Private/Templates/
        # cat=plugin.tx_hiveextapi_request/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_api/Resources/Private/Partials/
        # cat=plugin.tx_hiveextapi_request/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_api/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextapi_request//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveextapi_request {
        settings {
            api {
                request {
                    pid =
                    token = ab03c1995fa06e6d0ec8adf88ecc6666
                    typeNum = 666
                }
            }
        }
    }
}